## Docker containerları production için ayağa kaldırma
```bash
 docker-compose up 
```

## Production Docker containerları silme
```bash
 docker-compose down 
```

## Docker containerları development için ayağa kaldırma
```bash
 docker-compose -f docker-compose.dev.yml up 
```

## Development Docker containerları silme
```bash
 docker-compose -f docker-compose.dev.yml down 
```